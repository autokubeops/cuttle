package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/autokubeops/cuttle/internal/kn"
	"gitlab.com/autokubeops/cuttle/internal/regcred"
	"os"
)

func main() {
	app := Run()
	if err := app.Run(os.Args); err != nil {
		log.WithError(err).Fatal("failed to run")
	}
}

func Run() *cli.App {
	return &cli.App{
		Name:        "Cuttle",
		Description: "CLI for deploying applications to Kubernetes via GitLab CI",
		Commands: []*cli.Command{
			{
				Name:        "regcred",
				Description: "creates a Kubernetes ImagePullSecret",
				Flags:       regcred.Flags,
				Action:      regcred.Run,
			},
			{
				Name: "function",
				Subcommands: []*cli.Command{
					{
						Name:        "deploy",
						Description: "creates a Knative service",
						Flags:       kn.Flags,
						Action:      kn.Run,
					},
				},
			},
		},
	}
}
