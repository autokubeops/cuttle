package config

import (
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

func New(path string, v interface{}) error {
	fields := log.Fields{"file": path}
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to read file")
		return err
	}
	err = yaml.Unmarshal(data, v)
	if err != nil {
		log.WithError(err).WithFields(fields).Error("failed to unmarshal yaml")
		return err
	}
	return nil
}
