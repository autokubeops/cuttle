package runtime

import (
	"context"
	log "github.com/sirupsen/logrus"
	"os"
	"os/exec"
)

// NewRuntime creates a new Runtime
func NewRuntime(command string) *Runtime {
	return &Runtime{
		command: command,
	}
}

// Execute runs the specified command with the
// provided arguments
func (r *Runtime) Execute(ctx context.Context, args ...string) error {
	log.WithField("command", r.command).Info("executing command")
	envArgs := make([]string, len(args))
	for i := range args {
		envArgs[i] = os.ExpandEnv(args[i])
	}

	// run the command
	command := exec.CommandContext(ctx, r.command, envArgs...) //nolint:gosec
	command.Stdout = os.Stdout
	command.Stderr = os.Stderr

	if err := command.Run(); err != nil {
		log.WithError(err).Warning("could not execute command")
		return err
	}
	return nil
}
