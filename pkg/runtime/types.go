package runtime

import "context"

type Runtime struct {
	command string
}

type Runner interface {
	Execute(ctx context.Context, args ...string) error
}
