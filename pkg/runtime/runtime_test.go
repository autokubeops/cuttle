package runtime

import (
	"context"
	"github.com/stretchr/testify/assert"
	"testing"
)

// interface guard
var _ Runner = &Runtime{}

func TestNewRuntime(t *testing.T) {
	assert.NotNil(t, NewRuntime("echo"))
}

func TestRuntime_Execute(t *testing.T) {
	rt := NewRuntime("echo")
	assert.NoError(t, rt.Execute(context.TODO(), `"Hello, World!"`))
}

func TestRuntime_ExecuteFailure(t *testing.T) {
	rt := NewRuntime("false")
	assert.Error(t, rt.Execute(context.TODO(), "test"))
}
