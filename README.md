# Cuttle

Cuttle is a CLI application designed for reducing boilerplate when deploying to Kubernetes via GitLab CI.
Although it is designed for GitLab CI, it can be used elsewhere.

## Usage

Via a container image:

```yaml
deploy:
  image: registry.gitlab.com/autokubeops/cuttle:v0.1
  script:
    - cuttle regcred
    - cuttle function deploy
```

Directly:

Cuttle can be run directly, however it is recommended that you use a container as it provides all the required dependencies.
If you choose to build it manually, you will need to provide:
* `kubectl` - Kubernetes CLI
* `kn` - Knative CLI

## Commands

### regcred

Creates a Kubernetes ImagePullSecret using information from the environment.

```bash
cuttle regcred --help
```

### function

#### deploy

Creates a Knative service in a similar fashion to GitLab's AutoDeploy

```bash
cuttle function deploy --help
```

#### serverless.yaml

Function configuration is driven by the `serverless.yaml` file.

```yaml
# arbitrary knative arguments
# full list can be found at:
#   https://github.com/knative/client/blob/main/docs/cmd/kn_service_create.md#options
args:
  foo: bar
# arbitrary environment variables
# KEY=VALUE form
envs:
  FOO: bar
# additional options.
# None need to be specified as they use
# reasonable default values
opts:
  http2: false # enables h2c communication between the service and knative
  port: 8080 # port to run on
  skip_secret: false # skip creation of the image pull secret (e.g. if you are managing it yourself)
```