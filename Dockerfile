ARG KUBECTL_VERSION="1.22.4-debian-10-r10"
ARG GITLAB_PREFIX
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/go-git:1.17 as BUILDER

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp
WORKDIR /home/somebody/go

ARG GOPRIVATE

ARG AUTO_DEVOPS_GO_GIT_CFG
ARG AUTO_DEVOPS_GO_COMPILE_FLAGS

# copy our code in
COPY --chown=somebody:0 . .

# KANIKO_BUILD_ARGS=AUTO_DEVOPS_GO_GIT_CFG="url.git@github.com.insteadOf https://github.com"
RUN if [[ -n "${AUTO_DEVOPS_GO_GIT_CFG}" ]]; then git config --global $(echo "${AUTO_DEVOPS_GO_GIT_CFG}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g'); fi

# build the binary
RUN if [ -d "./cmd" ]; then BUILD_DIR="./cmd/..."; else BUILD_DIR="."; fi && \
	TMPDIR=/home/somebody/.tmp CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o main "${BUILD_DIR}"

FROM harbor.dcas.dev/docker.io/bitnami/kubectl:${KUBECTL_VERSION}

USER root
COPY --chown=1001:0 --from=BUILDER /home/somebody/go/main /usr/local/bin/cuttle
ADD --chown=1001:0 https://github.com/knative/client/releases/download/knative-v1.0.0/kn-linux-amd64 /usr/local/bin/kn
RUN chmod +x /usr/local/bin/cuttle && \
    chmod +x /usr/local/bin/kn

USER 1001
ENTRYPOINT ["bash"]