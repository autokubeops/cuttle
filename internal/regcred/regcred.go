package regcred

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/autokubeops/cuttle/pkg/runtime"
	"os"
	"strings"
)

var Flags = []cli.Flag{
	&cli.StringFlag{
		Name:     FlagServer,
		EnvVars:  []string{EnvRegistry},
		Required: true,
	},
	&cli.StringFlag{
		Name:     FlagUsername,
		EnvVars:  []string{EnvDeployUser, EnvRegistryUser},
		Required: true,
	},
	&cli.StringFlag{
		Name:     FlagPassword,
		EnvVars:  []string{EnvDeployPassword, EnvRegistryPassword},
		Required: true,
	},
}

func Run(c *cli.Context) error {
	if os.Getenv(EnvProjectVisibility) == "public" {
		log.Info("project is public - skipping regcred creation")
		return nil
	}
	log.Info("Creating regcred")
	run := runtime.NewRuntime("bash")
	args := []string{
		"-c",
		command(c),
	}
	return run.Execute(c.Context, args...)
}

func command(c *cli.Context) string {
	return strings.Join([]string{
		"kubectl create secret docker-registry",
		fmt.Sprintf("gitlab-registry-%s", os.Getenv(EnvProjectPathSlug)),
		fmt.Sprintf("--docker-server=%s", c.String(FlagServer)),
		fmt.Sprintf("--docker-username=%s", c.String(FlagUsername)),
		fmt.Sprintf("--docker-password=%s", c.String(FlagPassword)),
		"-o yaml --dry-run=client | kubectl replace",
		fmt.Sprintf("-n=%s", os.Getenv(EnvNamespace)),
		"--force -f -",
	}, " ")
}
