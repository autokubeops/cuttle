package regcred

const (
	EnvNamespace         = "KUBE_NAMESPACE"
	EnvProjectVisibility = "CI_PROJECT_VISIBILITY"
	EnvProjectPathSlug   = "CI_PROJECT_PATH_SLUG"
	EnvDeployUser        = "CI_DEPLOY_USER"
	EnvDeployPassword    = "CI_DEPLOY_PASSWORD" //nolint:gosec
	EnvRegistry          = "CI_REGISTRY"
	EnvRegistryUser      = "CI_REGISTRY_USER"
	EnvRegistryPassword  = "CI_REGISTRY_PASSWORD" //nolint:gosec
)

const (
	FlagServer   = "docker-server"
	FlagUsername = "docker-username"
	FlagPassword = "docker-password"
)
