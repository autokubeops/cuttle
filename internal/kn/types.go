package kn

const (
	flagConfig     = "config"
	flagPullSecret = "pull-secret"
	flagImageRepo  = "repository"
	flagImageTag   = "tag"
)

const (
	EnvNamespace       = "KUBE_NAMESPACE"
	EnvEnvironmentSlug = "CI_ENVIRONMENT_SLUG"
	EnvProjectSlug     = "CI_PROJECT_PATH_SLUG"
	EnvProjectID       = "CI_PROJECT_ID"
	EnvAppRepo         = "CI_APPLICATION_REPOSITORY"
	EnvAppTag          = "CI_APPLICATION_TAG"
	EnvRegistryImage   = "CI_REGISTRY_IMAGE"
	EnvCommitSha       = "CI_COMMIT_SHA"
	EnvKubePrefix      = "K8S_SECRET_"
)

const (
	PortHTTP1 = "http1"
	PortHTTP2 = "h2c"
)

type function struct {
	Args map[string]string `yaml:"args"`
	Envs map[string]string `yaml:"envs"`
	Opts opts              `yaml:"opts"`
}

type opts struct {
	HTTP2      bool `yaml:"http2"`
	Port       int  `yaml:"port"`
	SkipSecret bool `yaml:"skip_secret"`
}
