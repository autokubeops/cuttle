package kn

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/autokubeops/cuttle/internal/regcred"
	"gitlab.com/autokubeops/cuttle/pkg/config"
	"gitlab.com/autokubeops/cuttle/pkg/runtime"
	"os"
	"strings"
)

var Flags = []cli.Flag{
	&cli.StringFlag{
		Name:  flagConfig,
		Value: "serverless.yaml",
	},
	&cli.StringFlag{
		Name:  flagPullSecret,
		Value: fmt.Sprintf("gitlab-registry-%s", os.Getenv(regcred.EnvProjectPathSlug)),
	},
	&cli.StringFlag{
		Name:    flagImageRepo,
		EnvVars: []string{EnvAppRepo, EnvRegistryImage},
	},
	&cli.StringFlag{
		Name:    flagImageTag,
		EnvVars: []string{EnvAppTag, EnvCommitSha},
	},
}

func Run(c *cli.Context) error {
	var conf function
	if err := config.New(c.String(flagConfig), &conf); err != nil {
		return err
	}
	log.Info("Creating service")
	kn := runtime.NewRuntime("kn")
	name := fmt.Sprintf("%s-%s", os.Getenv(EnvEnvironmentSlug), os.Getenv(EnvProjectID))
	secretName := fmt.Sprintf("%s-secret", name)
	portName := PortHTTP1
	if conf.Opts.HTTP2 {
		portName = PortHTTP2
	}
	port := 8080
	if conf.Opts.Port > 0 {
		port = conf.Opts.Port
	}
	args := []string{
		"service",
		"create",
		name,
		fmt.Sprintf("--port=%s:%d", portName, port),
		fmt.Sprintf("-n=%s", os.Getenv(EnvNamespace)),
		"--force",
		fmt.Sprintf("--image=%s:%s", c.String(flagImageRepo), c.String(flagImageTag)),
		fmt.Sprintf("--annotation=app.gitlab.com/app=%s", os.Getenv(EnvProjectSlug)),
		fmt.Sprintf("--annotation=app.gitlab.com/env=%s", os.Getenv(EnvEnvironmentSlug)),
	}
	if os.Getenv(regcred.EnvProjectVisibility) != "public" && !conf.Opts.SkipSecret {
		log.Infof("Configuring secret: %s", c.String(flagPullSecret))
		args = append(args, fmt.Sprintf("--pull-secret=%s", c.String(flagPullSecret)))
	}
	for k, v := range conf.Args {
		args = append(args, fmt.Sprintf("--%s=%s", k, v))
	}
	// inject explicit environment variables (#1)
	for k, v := range conf.Envs {
		args = append(args, fmt.Sprintf("--env=%s=%s", k, v))
	}
	// try to create the app secret (#1)
	if err := createAppSecret(c.Context, secretName); err == nil {
		args = append(args, fmt.Sprintf("--env-from=secret:%s", secretName))
	} else {
		log.WithError(err).Error("failed to create application secret")
	}
	return kn.Execute(c.Context, args...)
}

// createAppSecret creates a Kubernetes Secret and pulls
// prefixed environment variables in.
//
// Any environment variable prefixed with K8S_SECRET_ will
// have the prefix stripped. E.g. K8S_SECRET_FOO=bar will
// become FOO=bar
func createAppSecret(ctx context.Context, name string) error {
	log.Info("Creating application secret")
	run := runtime.NewRuntime("bash")
	pairs := []string{
		"kubectl create secret generic",
		name,
	}
	// collect all the envs starting with our prefix
	for _, e := range os.Environ() {
		if strings.HasPrefix(e, EnvKubePrefix) {
			pairs = append(pairs, fmt.Sprintf("--from-literal=%s", strings.TrimPrefix(e, EnvKubePrefix)))
		}
	}
	pairs = append(pairs, []string{
		"-o yaml --dry-run=client | kubectl replace",
		fmt.Sprintf("-n=%s", os.Getenv(EnvNamespace)),
		"--force -f -",
	}...)
	// create the secret
	return run.Execute(ctx, []string{
		"-c",
		strings.Join(pairs, " "),
	}...)
}
